$(document).ready(function(){
	$.ajax({
		type: "GET",
		url: "http://ec2-54-187-142-30.us-west-2.compute.amazonaws.com:9000/getAllOffersInCity",
		data:{city:"Bangalore"}

	})
	.done(function(featuredData) {
		featuredData.featured.forEach(function(feature){
			console.log(feature);
			var html = _.template($('#featuredSlider').html(), {feature:feature});
			$('#owl-demo').append(html);
			$("#owl-demo").owlCarousel({
 
		      navigation : true, // Show next and prev buttons
		      slideSpeed : 300,
		      paginationSpeed : 400,
		      singleItem: true,
		      items : 3,
			  itemsCustom : false,
			  itemsDesktop : [1199,1],
			  itemsDesktopSmall : [980,1],
			  itemsTablet: [768,1],
			  itemsTabletSmall: false,
			  itemsMobile : [479,1],
			  singleItem : false,
			  itemsScaleUp : false,
			 
			    //Basic Speeds
			  slideSpeed : 200,
			  paginationSpeed : 800,
			  rewindSpeed : 1000,
			 
			    //Autoplay
			  autoPlay : false,
			  stopOnHover : false,
			 
			    // Navigation
			  navigation : false,
			  navigationText : ["prev","next"],
			  rewindNav : true,
			  scrollPerPage : false,
			 
			    //Pagination
			  pagination : true,
			  paginationNumbers: false,
			     // Responsive 
			  responsive: true,
			  responsiveRefreshRate : 200,
			  responsiveBaseWidth: window,
			     // CSS Styles
			  baseClass : "owl-carousel",
			  theme : "owl-theme",
			 
			    //Lazy load
			  lazyLoad : false,
			  lazyFollow : true,
			  lazyEffect : "fade",
			 
			    //Auto height
			  autoHeight : false,
			 
			    //JSON 
			  jsonPath : false, 
			  jsonSuccess : false,
			 
			    //Mouse Events
			  dragBeforeAnimFinish : true,
			  mouseDrag : true,
			  touchDrag : true,
			 	    //Transitions
			  transitionStyle : false,
			 
			    // Other
			  addClassActive : false,
			 
			    //Callbacks
			  beforeUpdate : false,
			  afterUpdate : false,
			  beforeInit: false, 
			  afterInit: false, 
			  beforeMove: false, 
			  afterMove: false,
			  afterAction: false,
			  startDragging : false,
			  afterLazyLoad : false

			 
			      // "singleItem:true" is a shortcut for:
			      // items : 1, 
			      // itemsDesktop : false,
			      // itemsDesktopSmall : false,
			      // itemsTablet: false,
			      // itemsMobile : false
			 
			});
			
		});
	
	});
  
	var ddlData = [
	{
	text: "Likes",
	value: 1,
	imageSrc: "images/heart-grey.png"
	},
	{
	text: "Views",
	value: 2,
	imageSrc: "images/view-grey.png"
	}
	];
	$('#ddimages').ddslick({
	data: ddlData,
	width: 170,
	selectText: "Select option",
	imagePosition: "left",
	onSelected: function(data) {
	//callback function: do something with selectedData;
		// console.log(data);
	}
	});

	$('.ui-li').click(function(event) {
  // console.log(event);
  event.stopPropagation();
  var txt = $(this).text();
  // alert('Selected Location = ' +txt);
  document.getElementById('selected-value').innerHTML = txt;
  // $( ".selected-value" ).append( "<strong>"+txt+",Bangalore.</strong>" );
  // $(".content-body").empty();

});

	$.ajax({
		type: "GET",
		url: "http://ec2-54-187-142-30.us-west-2.compute.amazonaws.com:9000/getAllCategories"
	})
	.done(function(categories) {
		categories.forEach(function(category){
			console.log(category);
			var html = _.template($('#cat').html(), {category:category});
			// document.getElementById('categories').innerHTML = html;
			$('#categories').append(html);
			
		});
	
	});

(function(){
	$.ajax({
		type: "GET",
		url: "http://ec2-54-187-142-30.us-west-2.compute.amazonaws.com:9000/getAllBiz"
	})
	.done(function(shopNames) {
		window.businesses = {};
		shopNames.bizs.forEach(function(business){
			window.businesses[business.bizId] = business;

		});
		console.log(window.businesses);
	});
})();

(function(){
	$.ajax({
		type: "GET",
		url: "http://ec2-54-187-142-30.us-west-2.compute.amazonaws.com:9000/getAllCategories"
	})
	.done(function(cateNames) {
		window.catDatas = {};
		cateNames.forEach(function(cateTitle){
			window.catDatas[cateTitle.catName] = cateTitle;

		});
		console.log(window.catDatas);
	});
})();

});

var viewCategoryOffers = function(id){
	localStorage.setItem('selectedCatId', id);
	console.log('selected catId===>',localStorage.selectedCatId);
	$.mobile.changePage('#categorydetail',{transition:"slide"});
}

var loadCategoryOffers = function(cat){
	console.log('will load cat Name===>'+cat);
	// $('#categoryName').empty();
	// $('#categoryName').append(cat);
	// document.getElementById('categoryName').innerHTML = cat;
	$.ajax({
		type: "GET",
		url: "http://ec2-54-187-142-30.us-west-2.compute.amazonaws.com:9000/getOffers",
		data : {city:'Bangalore', cat: cat, lat:22.3, lng:77.15, radius:10}

	})
	.done(function(data) {

		console.log('offers list===>',data.offers);
		data.offers.forEach(function(offer){
			var html = _.template($('#offer').html(), {offer:offer});
			document.getElementById('offersbox').innerHTML = html;
			
		});
	
	});
}

var viewOfferItem = function(id){
	localStorage.setItem('selectedOffId', id);
	console.log('viewOfferItem id===> '+id);
	$.mobile.changePage('#offer-detail', {transition:"slide"});
}

var viewOffersDetail = function(offerId){
	console.log('will load offerId===>'+offerId);
	$.ajax({
		type: "GET",
		url: "http://ec2-54-187-142-30.us-west-2.compute.amazonaws.com:9000/getOffer",
		data : {offerId: offerId}
	})
	.done(function(data) {
		data.offers.forEach(function(offersdetail){
			console.log('will load offers detail===>',offersdetail);
			var html = _.template($('#offersdetail').html(), {offersdetail:offersdetail});
			document.getElementById('offercontainer').innerHTML = html;
			$('.li_tab').on('click', function(event) {
  				$('.bgcolor').removeClass('bgcolor');
			  	$(this).addClass('bgcolor');
			  	var currentVal = $(this).attr('attr');
			  	$('.show').removeClass('show').addClass('hide');
			  	$('#'+currentVal).removeClass('hide').addClass('show');
			  

			});


		});
	
	});
}


var viewBiz = function(id){
	localStorage.setItem('selectedCatId', id);
	console.log('viewBiz of id===>'+id);
	$.mobile.changePage('#biz-detail', {transition:"slide"});
}

var viewBizDetail = function(catId){
	console.log('will load cat Id===>'+catId);
	$.ajax({
		type: "GET",
		url: "http://ec2-54-187-142-30.us-west-2.compute.amazonaws.com:9000/getAllBiz",
		data : {catId: catId}
	})
	.done(function(data) {
		data.bizs.forEach(function(biz){
			console.log(biz);
			var html = _.template($('#biz').html(), {biz:biz});
			document.getElementById('bizdetails').innerHTML = html;
			
		});
	
	});
}


var selectLocation = function(){
	$.mobile.changePage('#location',{transition:"slide"});
}
